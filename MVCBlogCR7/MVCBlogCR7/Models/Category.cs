﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBlogCR7.Models
{
    public class Category
    {
        public int categoryId { get; set; }
        public string categoryName { get; set; }

        public Category()
        {}

        public Category(int id, string name)
        {
            this.categoryId = id;
            this.categoryName = name;
        }
    }
}