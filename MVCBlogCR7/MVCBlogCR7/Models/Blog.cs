﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCBlogCR7.Models
{
    public class Blog
    {
        public int id { get; set; }
        public string title { get; set; }
        public string des { get; set; }
        public string position { get; set; }
        public string detail { get; set; }
        public int categoryId { get; set; }

        public System.DateTime datePublic { get; set; }
        public bool isPublic { get; set; }

        public List<string> listPosition { get; set; }
    }
}