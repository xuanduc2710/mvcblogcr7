﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBlogCR7.Models
{
    public class Position
    {
        public int positionId { get; set; }
        public string positionName { get; set; }

        public Position()
        {}

        public Position(int id, string name)
        {
            this.positionId = id;
            this.positionName = name;
        }
    }
}