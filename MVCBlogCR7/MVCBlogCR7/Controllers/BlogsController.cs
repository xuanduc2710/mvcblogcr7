﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBlogCR7.Data;
using MVCBlogCR7.Models;

namespace MVCBlogCR7.Controllers
{
    public class BlogsController : Controller
    {
        private MVCBlogCR7Context db = new MVCBlogCR7Context();
        List<Category> categories = new DataList().ListCategory;
        List<Position> positions = new DataList().ListPostion;

        // GET: Blogs
        public ActionResult Index()
        {
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            return View(db.Blogs.ToList());
        }

        public ActionResult Search(string keySearch)
        {
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            if (!String.IsNullOrEmpty(keySearch))
            {
                return View(db.Blogs.ToList().Where(s => s.title.Contains(keySearch)));
            }
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            return View(db.Blogs.ToList());
        }

        // GET: Blogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // GET: Blogs/Create
        public ActionResult Create()

        {
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            return View();
        }

        // POST: Blogs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Blog blog)
        {
            blog.position = String.Join(",", blog.listPosition);
            if (ModelState.IsValid)
            {
                ViewBag.categoryList = categories;
                ViewBag.positionList = positions;
                db.Blogs.Add(blog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            return View(blog);
        }

        // GET: Blogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }

            ViewBag.checkedPosition = blog.position.Split(',', ' ');
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            return View(blog);
        }

        // POST: Blogs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Blog blog)
        {
            blog.position = String.Join(",", blog.listPosition);
            if (ModelState.IsValid)
            {
                ViewBag.categoryList = categories;
                ViewBag.positionList = positions;
                db.Entry(blog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoryList = categories;
            ViewBag.positionList = positions;
            return View(blog);
        }

        // GET: Blogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: Blogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Blog blog = db.Blogs.Find(id);
            db.Blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
